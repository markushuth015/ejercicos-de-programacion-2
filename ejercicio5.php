<!DOCTYPE html>
<html>
<style>
      tr{
        background-color: #FFFFFF;
      }
      tr:nth-child(odd) {
        background-color: #CCCCCC;
      }

</style>
<body>


<?php
	$var = 9;    
    echo "<table border ='1'><br>";
    echo "<h3>Tabla de multiplicar del 9</h3>";
    
    for ($i=1; $i <= 10; $i++)
    	{
        	$valor = $i*$var;
            echo "<tr>";
            echo "<td> $i X $var </td>";
            echo "<td> $valor </td>";
            echo "</tr>";
		}
        
        echo "</table>";

?>

</body>
</html>
